﻿using System;
using WikeSoft.Data.Models.Filters;

namespace WikeSoft.Enterprise.Models.Filters.Sys
{
    /// <summary>
    /// 查询过滤器
    /// </summary>
    public class DataDictionaryFilter : BaseFilter
    {
        /// <summary>
        /// 分组码
        /// </summary>
        public String GroupCode { get; set; }

        public String DictionayName { get; set; }
    }
}
