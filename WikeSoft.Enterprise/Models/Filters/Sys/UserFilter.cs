﻿using System;
using WikeSoft.Data.Models.Filters;

namespace WikeSoft.Enterprise.Models.Filters.Sys
{
    /// <summary>
    /// 查询过滤器
    /// </summary>
    public class UserFilter : BaseFilter
    {
        public String UserName { get; set; }

        public String TrueName { get; set; }

        public string DepartmentId { get; set; }
    }
}
