﻿using System;
using Newtonsoft.Json;

namespace WikeSoft.Enterprise.Models
{
    /// <summary>
    /// ztree页面模型
    /// </summary>
    public class ZTreeModel
    {
        /// <summary>
        /// Id
        /// </summary>
        [JsonProperty("id")]
        public string id { get; set; }

        /// <summary>
        /// 父节点Id
        /// </summary>
        public string pId { get; set; }

        /// <summary>
        /// 页面名称
        /// </summary>
        [JsonProperty("name")]
        public string name { get; set; }

        /// <summary>
        ///  是否是父节点
        /// </summary>
        [JsonProperty("isParent")]
        public bool isParent { get; set; }

        /// <summary>
        /// 是否展开
        /// </summary>
        public bool open { get; set; }
    }
}
