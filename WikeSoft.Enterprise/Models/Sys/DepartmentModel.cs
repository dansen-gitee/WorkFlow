﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WikeSoft.Enterprise.Models.Sys
{

    public class DepartmentModel
    {
        ///<summary>
        /// 主键
        ///</summary>
        public string Id { get; set; } // Id (Primary key)

        ///<summary>
        /// 部门名称
        ///</summary>

        [Display(Name = "部门名称")]
        public string DepartmentName { get; set; } // DepartmentName (length: 500)

        ///<summary>
        /// 部门编码
        ///</summary>
        [Display(Name = "部门编码")]
        public string DepartmentCode { get; set; } // DepartmentCode (length: 500)

        ///<summary>
        /// 上级节点
        ///</summary>
        public string ParentId { get; set; } // ParentId

        /// <summary>
        /// 上级部门名称
        /// </summary>
        [Display(Name = "上级部门名称")]
        public String ParentName { get; set; }

        ///<summary>
        /// 说明
        ///</summary>
        [Display(Name = "说明")]
        public string Remark { get; set; } // Remark (length: 500)

        
    }
    public class DepartmentAddModel
    {

        ///<summary>
        /// 部门名称
        ///</summary>
        [Display(Name = "部门名称")]
        [Required(ErrorMessage = ModelErrorMessage.Required)]
        public string DepartmentName { get; set; } // DepartmentName (length: 500)

        ///<summary>
        /// 部门编码
        ///</summary>
        [Display(Name = "部门编码")]
        public string DepartmentCode { get; set; } // DepartmentCode (length: 500)

        ///<summary>
        /// 上级节点
        ///</summary>
        [Display(Name = "上级节点")]
        public string ParentId { get; set; } // ParentId

        /// <summary>
        /// 上级部门名称
        /// </summary>
        [Display(Name = "上级部门名称")]
        public String ParentName { get; set; }

        ///<summary>
        /// 说明
        ///</summary>
        [Display(Name = "说明")]
        public string Remark { get; set; } // Remark (length: 500)

        ///<summary>
        /// 所属区域
        ///</summary>
        //[Required(ErrorMessage = ModelErrorMessage.Required)]
        [Display(Name = "所属区域")]
        public string Area { get; set; } // Area




    }

    public class DepartmentEditModel
    {
        ///<summary>
        /// 主键
        ///</summary>
        public string Id { get; set; } // Id (Primary key)

        ///<summary>
        /// 部门名称
        ///</summary>
        [Display(Name = "部门名称")]
        [Required(ErrorMessage = ModelErrorMessage.Required)]
        public string DepartmentName { get; set; } // DepartmentName (length: 500)

        ///<summary>
        /// 部门编码
        ///</summary>
        [Display(Name = "部门编码")]
        public string DepartmentCode { get; set; } // DepartmentCode (length: 500)

        ///<summary>
        /// 上级节点
        ///</summary>
        [Display(Name = "上级节点")]
        public string ParentId { get; set; } // ParentId

        /// <summary>
        /// 上级部门名称
        /// </summary>
        [Display(Name = "上级部门名称")]
        public String ParentName { get; set; }

        ///<summary>
        /// 说明
        ///</summary>
        [Display(Name = "说明")]
        public string Remark { get; set; } // Remark (length: 500)

        ///<summary>
        /// 所属区域
        ///</summary>
        //[Required(ErrorMessage = ModelErrorMessage.Required)]
        [Display(Name = "所属区域")]
        public string Area { get; set; } // Area

    }
}
