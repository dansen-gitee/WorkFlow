﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using WikeSoft.Enterprise.Entities;
using WikeSoft.Enterprise.Enum;

namespace WikeSoft.Enterprise.Models.Sys
{
    /// <summary>
    /// 用户模型
    /// </summary>
    public class UserModel
    {
        ///<summary>
        /// 主键
        ///</summary>
        public string Id { get; set; } // Id (Primary key)

        ///<summary>
        /// 用户名，登录名
        ///</summary>
        public string UserName { get; set; } // UserName (length: 500)

        ///<summary>
        /// 密码
        ///</summary>
        public string PassWord { get; set; } // PassWord (length: 500)

        ///<summary>
        /// 真实姓名
        ///</summary>
        public string TrueName { get; set; } // TrueName (length: 500)

        ///<summary>
        /// 邮箱
        ///</summary>
        public string Email { get; set; } // Email (length: 500)

        ///<summary>
        /// 手机
        ///</summary>
        public string Mobile { get; set; } // Mobile (length: 500)

        ///<summary>
        /// 用户状态（可用，不可用）
        ///</summary>
        public UserStatus? UserStatus { get; set; } // UserStatus

        /// <summary>
        /// 所属岗位名称
        /// </summary>
        public string PostName { get; set; }

        ///<summary>
        /// 部门ID
        ///</summary>
        public string DepartmentId { get; set; } // DepartmentId

        /// <summary>
        /// 部门名称
        /// </summary>
        public String DepartmentName { get; set; }
        ///<summary>
        /// 员工编号
        ///</summary>
        public string UserCode { get; set; } // UserCode (length: 100)

        public bool IsDelete { get; set; }
    }

    /// <summary>
    /// 添加用户模型
    /// </summary>
    public class UserAddModel
    {
        [Display(Name = "用户名")]
        [Required(ErrorMessage = ModelErrorMessage.Required)]
        [MaxLength(20, ErrorMessage = ModelErrorMessage.MaxLength)]
        [Remote("IsExists", "User", ErrorMessage = ModelErrorMessage.Exists)]
        public string UserName { get; set; } // UserName (length: 500)

        ///<summary>
        /// 密码
        ///</summary>
        [Display(Name = "密码")]
        [Required(ErrorMessage = ModelErrorMessage.Required)]
        [MaxLength(20, ErrorMessage = ModelErrorMessage.MaxLength)]
        public string PassWord { get; set; } // PassWord (length: 500)

        ///<summary>
        /// 真实姓名
        ///</summary>
        [Display(Name = "真实姓名")]
        [MaxLength(20, ErrorMessage = ModelErrorMessage.MaxLength)]
        [Required(ErrorMessage = ModelErrorMessage.Required)]
        public string TrueName { get; set; } // TrueName (length: 500)

        ///<summary>
        /// 邮箱
        ///</summary>
        [Display(Name = "邮箱")]
        [MaxLength(50, ErrorMessage = ModelErrorMessage.MaxLength)]
        [RegularExpression(RegularRule.Email, ErrorMessage = ModelErrorMessage.Regular)]
        public string Email { get; set; } // Email (length: 500)

        ///<summary>
        /// 手机
        ///</summary>
        [Display(Name = "手机")]
        [MaxLength(11, ErrorMessage = ModelErrorMessage.MaxLength)]
        [RegularExpression(RegularRule.Phone, ErrorMessage = ModelErrorMessage.Regular)]
        public string Mobile { get; set; } // Mobile (length: 500)
 

        /// <summary>
        /// 用户状态
        /// </summary>
        [Display(Name = "用户状态")]
        [Required(ErrorMessage = ModelErrorMessage.Required)]
        public UserStatus UserStatus { get; set; }

        ///<summary>
        /// 部门ID
        ///</summary>
        public string DepartmentId { get; set; } // DepartmentId

        /// <summary>
        /// 部门名称
        /// </summary>
        [Display(Name = "所属部门")]
        //[Required(ErrorMessage = ModelErrorMessage.Required)]
        public String DepartmentName { get; set; }

        ///<summary>
        /// 员工编号
        ///</summary>
        [Display(Name = "员工编号")]
        [Remote("IsExsitUserCode", "User", AdditionalFields = "Id", ErrorMessage = "员工编号已存在")]
        public string UserCode { get; set; } // UserCode (length: 100)

        /// <summary>
        /// 角色ID集合
        /// </summary>
        public IList<string> RoleIds { get; set; }
    }

    /// <summary>
    /// 编辑用户模型
    /// </summary>
    public class UserEditModel
    {
        [Required(ErrorMessage = "请输入Id")]
        public string Id { get; set; }

        [Display(Name = "用户名")]
        [MaxLength(20, ErrorMessage = ModelErrorMessage.MaxLength)]
        [Required(ErrorMessage = ModelErrorMessage.Required)]
        [Remote("IsExists", "User",AdditionalFields = "Id", ErrorMessage = "登录名已存在")]
        public string UserName { get; set; } // UserName (length: 500)

        ///<summary>
        /// 密码
        ///</summary>
        [Display(Name = "密码")]
        [MaxLength(20, ErrorMessage = ModelErrorMessage.MaxLength)]
        public string PassWord { get; set; } // PassWord (length: 500)

        ///<summary>
        /// 真实姓名
        ///</summary>
        [Display(Name = "真实姓名")]
        [Required(ErrorMessage = ModelErrorMessage.Required)]
        [MaxLength(20, ErrorMessage = ModelErrorMessage.MaxLength)]
        public string TrueName { get; set; } // TrueName (length: 500)

        ///<summary>
        /// 邮箱
        ///</summary>
        [Display(Name = "邮箱")]
        [MaxLength(50, ErrorMessage = ModelErrorMessage.MaxLength)]
        [RegularExpression(RegularRule.Email, ErrorMessage = ModelErrorMessage.Regular)]
        public string Email { get; set; } // Email (length: 500)

        ///<summary>
        /// 手机
        ///</summary>
        [Display(Name = "手机")]
        [MaxLength(11, ErrorMessage = ModelErrorMessage.MaxLength)]
        [RegularExpression(RegularRule.Phone, ErrorMessage = ModelErrorMessage.Regular)]
        public string Mobile { get; set; }

        ///<summary>
        /// 用户密码过期日期
        ///</summary>
        public System.DateTime? PasswordExpirationDate { get; set; } // PasswordExpirationDate

        /// <summary>
        /// 用户状态
        /// </summary>
        [Display(Name = "用户状态")]
        [Required(ErrorMessage = ModelErrorMessage.Required)]
        public UserStatus UserStatus { get; set; }

        /// <summary>
        /// 角色ID集合
        /// </summary>
        public IList<string> RoleIds { get; set; }

        ///<summary>
        /// 部门ID
        ///</summary>
        public string DepartmentId { get; set; } // DepartmentId

        /// <summary>
        /// 部门名称
        /// </summary>
        [Display(Name = "所属部门")]
        //[Required(ErrorMessage = ModelErrorMessage.Required)]
        public String DepartmentName { get; set; }

        ///<summary>
        /// 员工编号
        ///</summary>
        [Display(Name = "员工编号")]
        [Remote("IsExsitUserCode", "User", AdditionalFields = "Id", ErrorMessage = "员工编号已存在")]
        public string UserCode { get; set; } // UserCode (length: 100)
    }

    /// <summary>
    /// 登录模型
    /// </summary>
    public class LoginModel
    {
        /// <summary>
        /// 用户名
        /// </summary>
        [Display(Name = "用户名")]
        [Required(ErrorMessage = ModelErrorMessage.Required)]
        [MaxLength(100, ErrorMessage = ModelErrorMessage.MaxLength)]
        public string UserName { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        [Display(Name = "密码")]
        [Required(ErrorMessage = ModelErrorMessage.Required)]
        [MaxLength(100, ErrorMessage = ModelErrorMessage.MaxLength)]
        public string Password { get; set; }

        /// <summary>
        /// 来源地址
        /// </summary>
        public string ReturnUrl { get; set; }

        /// <summary>
        /// 是否登录成功
        /// </summary>
        public bool LoginSuccess { get; set; }

        /// <summary>
        /// 登录失败消息
        /// </summary>
        public string LoginError { get; set; }

        /// <summary>
        /// 登陆后用户的ID
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// 真实姓名
        /// </summary>
        public string TrueName { get; set; }

         

        public List<SysRole> Roles { get; set; }
    }

    /// <summary>
    /// 更改用户密码模型
    /// </summary>
    public class ChangePasswordModel
    {
        /// <summary>
        /// 用户Id
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// 旧密码
        /// </summary>
        [Display(Name = "旧密码")]
        [Required(ErrorMessage = ModelErrorMessage.Required)]
        [Remote("IsPasswordCorrect", "User", ErrorMessage = ModelErrorMessage.UnCorrectPassword)]
        public string OldPassword { get; set; }

        /// <summary>
        /// 新密码
        /// </summary>
        [Display(Name = "新密码")]
        [Required(ErrorMessage = ModelErrorMessage.Required)]
        [RegularExpression(@"(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{8,})$", ErrorMessage = ModelErrorMessage.RegularPassword)]
        [Remote("IsOldPassword", "User", ErrorMessage = ModelErrorMessage.IsOldPassword)]
        public string Password { get; set; }

        /// <summary>
        /// 确认密码
        /// </summary>
        [Display(Name = "确认密码")]
        [Required(ErrorMessage = ModelErrorMessage.Required)]
        [RegularExpression(@"(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{8,})$", ErrorMessage = ModelErrorMessage.RegularPassword)]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = ModelErrorMessage.NotSame)]
        public string ConfirmPassword { get; set; }
    }
}
