﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WikeSoft.Data.Enum
{
   
    public enum ACertificateTypeEnum
    {

        /// <summary>
        /// 带A
        /// </summary>
        [Description("带A")]
        HaveA = 0,

        /// <summary>
        /// 考A
        /// </summary>
        [Description("考A")]
        ExamA = 1,

        /// <summary>
        /// 考A
        /// </summary>
        [Description("不考A")]
        NoExamA = 2,
 
    }
}
