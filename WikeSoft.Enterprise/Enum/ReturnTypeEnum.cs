﻿using System.ComponentModel;

namespace WikeSoft.Data.Enum
{
    /// <summary>
    /// 归还类型
    /// </summary>
    public enum ReturnTypeEnum
    {
        /// <summary>
        /// 人才部还证
        /// </summary>
        [Description("人才部还证")]
        CustomerDepartment = 0,

        /// <summary>
        /// 企业部还证
        /// </summary>
        [Description("企业部还证")]
        CompanyDepartment = 1,

      
    }
}
